const L = require("leaflet")
require("leaflet.locatecontrol")

var overpassFrontend = new OverpassFrontend('//overpass-api.de/api/interpreter')

const attributions = {
    osm: '<a href="http://www.openstreetmap.org/copyright" target="_blank">OpenStreetMap</a>',
    thunderforest: '<a href="http://thunderforest.com/" target="_blank">Thunderforest</a>',
    ign: '<a href="http://www.ign.es/" target="_blank">Infraestructura de Datos Espaciales de Espa&ntilde;a (IDEE)</a>',
};

const layers = {
    'OpenStreetMap': L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: `&copy; ${attributions.osm}`
    }),
    'OpenCycleMap': L.tileLayer(
        'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=6de22730e2e34c4f9a0a002ff8f8cc57', {
        maxZoom: 19,
        attribution: `&copy; ${attributions.osm} & ${attributions.thunderforest}`,
    }),
    'Outdoors': L.tileLayer(
        'https://tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=6de22730e2e34c4f9a0a002ff8f8cc57', {
        maxZoom: 19,
        attribution: `&copy; ${attributions.osm} & ${attributions.thunderforest}`,
    }),
    'PNOA': L.tileLayer(
        'https://www.ign.es/wmts/pnoa-ma?service=WMTS&request=GetTile&version=1.0.0&Format=image/png&layer=OI.OrthoimageCoverage&style=default&tilematrixset=GoogleMapsCompatible&TileMatrix={z}&TileRow={y}&TileCol={x}', {
        minZoom: 4,
        maxZoom: 20,
        attribution: `&copy; ${attributions.ign}`,
        bounds: [
            [22.173559281306314, -47.0716243806546],
            [66.88067635831743,
                40.8749629405498
            ]
        ]
    }),
    'IGN Topo': L.tileLayer(
        "https://ign.es/wmts/mapa-raster?service=WMTS&request=GetTile&version=1.0.0&Format=image/jpeg&layer=MTN&style=default&style=default&tilematrixset=GoogleMapsCompatible&TileMatrix={z}&TileRow={y}&TileCol={x}", {
        minZoom: 4,
        maxZoom: 18,
        attribution: `&copy; ${attributions.ign}`,
        bounds: [
            [22.173559281306314, -47.0716243806546],
            [66.88067635831743,
                40.8749629405498
            ]
        ]
    }),
    // 'IGN Base': L.tileLayer(
    //     "https://www.ign.es/wmts/ign-base?service=WMTS&request=GetTile&version=1.0.0&Format=image/png&layer=IGNBaseOrto&style=default&style=default&tilematrixset=GoogleMapsCompatible&TileMatrix={z}&TileRow={y}&TileCol={x}", {
    //     bounds: [
    //         [27, -19],
    //         [44, 5]
    //     ],
    //     minZoom: 5
    // }),
};

const iconHTML = (name) => `<img src='icons/${name}.svg' width='20' height='20' anchorX='10' anchorY='10' signAnchorX='0' signAnchorY='-30' />`

// const overlays = { "cap": L.LayerGroup([innsbruck]).addTo(map) };

const gmapsLink = coords => `https://www.google.com/maps/@${coords},3z`

const mkBody = (obj) => {
    let lines = []
    Object.keys(obj.tags).map((tag) => {
        const v = obj.tags[tag]
        if (tag == "name") return;
        // else if (tag.startsWith("addr")) return;
        else if (tag == "fee" && v == "yes")
            lines.push(`<span style="color:red;">${tag}: ${v}</span>`)
        else if (tag == "website")
            lines.push(`${tag}: <a href="${v}" target="_blank">${v}</a>`)
        else if (tag == "wikipedia")
            lines.push(`${tag}: <a href="https://wikipedia.org/wiki/${v}" target="_blank">${v}</a>`)
        else if (tag == "wikidata")
            lines.push(`${tag}: <a href="https://www.wikidata.org/wiki/${v}" target="_blank">${v}</a>`)
        else if (tag == "wikimedia_commons")
            lines.push(`${tag}: <a href="https://commons.wikimedia.org/wiki/${v}" target="_blank">${v}</a>`)
        else
            lines.push(`${tag}: ${v}`)
    })
    lines.push(`
        <a href="https://openstreetmap.org/${obj.type}/${obj.osm_id}" target="_blank">osm</a>
    `)
    if (obj.tags.name) {
        lines.push(`<a href="https://duckduckgo.com/?q=${obj.tags.name}" target="_blank">buscar en duckduckgo</a>`)
    }
    return lines.join("</br>")
}

const mkOverlay = (icon, key, tag, minZoom = 11) =>
    new OverpassLayer({
        overpassFrontend: overpassFrontend,
        query: `(
            node[${key}~"^(${tag})$"];
            way[${key}~"^(${tag})$"];
            relation[${key}~"^(${tag})$"];
        )`,
        minZoom: minZoom,
        feature: {
            title: '{{ tags.name }}',
            style: { width: 0, color: 'transparent' },
            markerSymbol: iconHTML(icon),
            body: mkBody
        }
    })

const overlays = {
    "Agua potable": mkOverlay('water', 'amenity', 'drinking_water'),
    "Refugios": L.layerGroup([
        mkOverlay('basic-hut', 'amenity', 'wilderness_hut|alpine_hut'),
        mkOverlay('basic-hut', 'tourism', 'wilderness_hut|alpine_hut'),

    ]),
    "Abrigos": mkOverlay('shelter', 'amenity', 'shelter', 10),
    "Mesas": L.layerGroup([
        mkOverlay('picnic-table', 'tourism', 'picnic_site'),
        mkOverlay('picnic-table', 'leisure', 'picnic_table'),
    ]),
    "Baños": mkOverlay('toilets', 'amenity', 'toilets', 10),
    "Duchas": mkOverlay('shower', 'amenity', 'shower', 10),
    "Campings": mkOverlay('camping', 'tourism', 'camp_site', 10),
    "Tiendas": L.layerGroup([
        mkOverlay('bakery', 'shop', 'bakery'),
        mkOverlay('convenience', 'shop', 'convenience'),
        mkOverlay('greengrocer', 'shop', 'farm|greengrocer'),
        mkOverlay('supermarket', 'shop', 'supermarket'),
    ], { minZoom: 11 }),
    "Lavanderías": L.layerGroup([
        mkOverlay('laundry', 'shop', 'laundry'),
        mkOverlay('laundry', 'amenity', 'washing_machine')
    ]),
}


const decodeHash = (hash, defaultZoom = 4) => {
    if (hash == null) return;

    const parts = hash.split(',')
    try {
        const state = { lat: null, lng: null, zoom: defaultZoom }
        if (parts.length >= 2) {
            state.lat = parseFloat(parts[0])
            state.lng = parseFloat(parts[1])
            if (parts.length >= 3) {
                state.zoom = parseInt(parts[2])
            }
            return state;
        } else {
            return;
        }
    }
    catch (e) {
        return;
    }
}

const loadState = (defaultZoom = 4) => {
    const locationHash = decodeHash(window.location.hash.substring(1), defaultZoom)
    if (locationHash) return locationHash;
    else {
        const localStorageHash = decodeHash(localStorage.getItem('hash'), defaultZoom)
        return localStorageHash
    }
}

const saveState = () => {
    const hash = `${map.getCenter().lat.toFixed(6)},${map.getCenter().lng.toFixed(6)},${map.getZoom()}`
    window.location.hash = hash;
    localStorage.setItem('hash', hash)
}

const initialState = loadState() || { lat: 46, lng: 13, zoom: 4 }

const map = L.map('map', {
    center: [initialState.lat, initialState.lng],
    zoom: initialState.zoom,
    attributionControl: false
});
saveState()

L.control.attribution().setPrefix('<a href="https://leafletjs.com/" target="_blank">Leaflet</a>').addTo(map);


map.on("zoomend moveend", saveState)

// overlays.Refugios.addTo(map)
// overlays.Mesas.addTo(map)

L.control.locate().addTo(map)
L.control.layers(layers, overlays, { collapsed: true }).addTo(map);

layers.OpenCycleMap.addTo(map);

// L.Control.MyButton = L.Control.extend({
//     options: {
//         position: 'topright'
//     },
//     onAdd: function(map) {
//         var container = L.DomUtil.create('div', 'leaflet-bar leaflet-control');
//         var button = L.DomUtil.create('a', 'leaflet-control-button', container);
//         L.DomEvent.disableClickPropagation(button);
//         L.DomEvent.on(button, 'click', function() {
//             console.log('click');
//         });

//         container.title = "Title";

//         return container;
//     },
//     onRemove: function(map) { },
// });
// var control = new L.Control.MyButton()
// control.addTo(map);
